# SnugComponents / Form control

This is component wrapper for Nette forms.

## Content
<!-- TOC -->
- [Content](#content)
- [Setup](#setup)
- [Usage](#usage)
    * [Injecting/using](#injecting/using)
- [Setting](#setting)
    * [Own template](#own-template)
    * [Default values](#default-values)
    * [Translations](#translations)
    * [Ajax](#ajax)
    * [Own callbacks](#own-callbacks)
    * [Security](#security)
<!-- TOC -->
## Setup

```neon
extensions:
	SnugcomponentsForm: Snugcomponents\Form\FormExtension
```

## Usage

### Injecting/using

You can simply inject factory in Your Presenters/Controls:

```php
public function __construct(
    private readonly \Snugcomponents\Form\FormControlFactory $formControlFactory,
) {
    parent::__construct();
    ...
}
```
And then use it: (minimal setting)

```php
public function createComponentForm(): \Snugcomponents\Form\FormControl
{
    return $this->formControlFactory->create($this->userFormFactory);
}
```

## Setting

### Own template

You can set your own template to render form by calling `setTemplateFile` on `FormControl`
How to create own template you can find in [examples folder](../src/Examples/templates/bootstrap4.latte)

### Form factory

You need to provide your own form factory. This extension provides only rendering and handling your form in a component.
Provided instance of Form factory must implement `Snugcomponents\Form\FormFactory`, which has only one method create, 
which accepts object contain default values and its return value must be instance of `Nette\Application\UI\Form`.
Other setting of form factory is up to you, but it is recommended to define there success callback which saves data,
send e-mail etc. 

Example how to implement your factory you can find in [examples folder](../src/Examples/FormFactory.php)

### Default values

Method `create` from `FormControlFactory` has optional parameter `resourceEntity` which is type of `Nette\Security\Resource`,
so you can paste your own implementation of entity. It needs to be `Nette\Security\Resource` because of future security reasons.
If you past an object into this param, then this object will provide default values to the form.

### Translations

This extension automatically translate template.
Form is not translated, because you need to do it in your form factory.

### Ajax

This extension fully supports ajax requests. Please feel free to use any js ajax library.

### Own callbacks

You can set extra onSuccess callbacks to the `FormControl`. 
It is so, because of you need to redirect/flash-message after success saving.
Unfortunately form factory cannot do that, because callback need to reflect concrete
place in code. So if you use it in presenter, then specific presenter flash-message or redirect needs to be made.

Because of that you can set your extra callback in presenter/control like this:
```php
public function createComponentForm(): \Snugcomponents\Form\FormControl
{
    $form = $this->formControlFactory->create($this->userFormFactory);
    
    $form->onSuccess[] = fn() => $this->flashMessage('Successfuly saved', 'succes');
    // or
    $form->onSuccess[] = fn() => $this->redirect('this');
    // or both..
    
    return $form;
}
```

### Security

You cannot allow any user, who have not rights to add/edit specific element, access to this component.
Also, you need to secure this component from calling from other actions, then you want it to.

To do this the right way see [example implementation of presenter trait](../src/Examples/PresenterTrait.php)
