<?php declare(strict_types = 1);

namespace Snugcomponents\Form\Examples;

use Nette\DI\Attributes\Inject;
use Nette\Http\IResponse;
use Snugcomponents\Form\FormControl;
use Snugcomponents\Form\FormControlFactory;

// phpcs:ignore SlevomatCodingStandard.Classes.SuperfluousTraitNaming.SuperfluousSuffix
trait PresenterTrait
{

	#[Inject]
	public FormControlFactory $formControlFactory;

	#[Inject]
	public FormFactory $userFormFactory; // Your own implementation of FormFactory interface

	/**
	 * Security property, which ensure that no one who have no permission will not enter to componens
	 * You need to explicitly allow that in action, which in you need this component.
	 */
	private bool $canCreateComponentUserCreateForm = false;

	/**
	 * Security method. Should be in all of your presenters.
	 */
	private function checkSignalPrivilege(bool $isAllowedToCreate): void
	{
		if ($isAllowedToCreate) {
			// hacking... You need to log info about hacker.
			return;
		}

		$this->error('You are not allowed to view this page.', IResponse::S401_UNAUTHORIZED);
	}

	/**
	 * Creation of that component. You need to check privilege first, then create component,
	 * set own callbacks and at last return it.
	 */
	public function createComponentUserCreateForm(): FormControl
	{
		$this->checkSignalPrivilege($this->canCreateComponentUserCreateForm);

		$formControl = $this->formControlFactory->create($this->userFormFactory);

		$formControl->onSuccess[] = $this->onUserFormFactorySuccess(...);

		return $formControl;
	}

	/**
	 * Own callback for handling this concrete instance of this component in this concrete presenter.
	 */
	private function onUserFormFactorySuccess(): void
	{
		$this->flashMessage('Changes was successfully saved.', 'success');
		$this->redirect('this');
	}

}
