<?php declare(strict_types = 1);

namespace Snugcomponents\Form\Examples;

use Nette\Application\UI\Form;
use Nette\Security\Resource;

class FormFactory implements \Snugcomponents\Form\FormFactory
{

	public function create(Resource|null $resourceEntity): Form
	{
		$form = new Form();
		$form->getElementPrototype()->setAttribute('novalidate', 'novalidate');

		$form->addText('name', 'Name:')
			->setRequired();

		$form->addSubmit('submit', 'Save');

		/* @phpstan-ignore-next-line */
		$form->onSuccess[] = $this->onSuccess(...);

		return $form;
	}

	private function onSuccess(Form $form): void
	{
        // phpcs:ignore SlevomatCodingStandard.Variables.UnusedVariable.UnusedVariable
		$values = $form->getValues();
		// Do stuff like saving or sending email, etc.
	}

}
