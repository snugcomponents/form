<?php declare(strict_types = 1);

namespace Snugcomponents\Form;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;
use Nette\Bridges\ApplicationLatte\Template;
use Nette\Security\Resource;
use function array_merge;
use function assert;

class FormControl extends Control
{

	private string $templateFile = __DIR__ . '/Examples/templates/bootstrap4.latte';

	/** @var array<callable(Form, array|object): void|callable(array|object): void>  Occurs when form success */ /** @phpstan-ignore-line */
	public array $onSuccess = []; /** @phpstan-ignore-line */

	/** @var array<mixed> */
	public array $templateData = [];

	/**
	 * Constructor
	 */
	public function __construct(
		private readonly FormFactory $formFactory,
		private readonly Resource|null $resourceEntity = null,
	)
	{
	}

	public function setTemplateFile(string $templateFile): static
	{
		$this->templateFile = $templateFile;

		return $this;
	}

	public function render(): void
	{
		$template = $this->getTemplate();
		assert($template instanceof Template);

		$this->templateData['resourceEntity'] = $this->resourceEntity;

		$template->render($this->templateFile, $this->templateData);
	}

	public function createComponentForm(): Form
	{
		$form = $this->formFactory->create($this->resourceEntity);

		$form->onSubmit[] = $this->onSubmit(...);
		$form->onSuccess = array_merge($form->onSuccess, $this->onSuccess);

		return $form;
	}

	private function onSubmit(): void
	{
		$this->redrawControl('form');
	}

}
