<?php declare(strict_types = 1);

namespace Snugcomponents\Form;

use Nette\Security\Resource;

interface FormControlFactory
{

	public function create(
		FormFactory $formFactory,
		Resource|null $resourceEntity = null,
	): FormControl;

}
