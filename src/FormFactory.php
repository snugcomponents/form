<?php declare(strict_types = 1);

namespace Snugcomponents\Form;

use Nette\Application\UI\Form;
use Nette\Security\Resource;

interface FormFactory
{

	public function create(Resource|null $resourceEntity): Form;

}
